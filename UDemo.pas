unit UDemo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, JvExExtCtrls, JvNavigationPane,
  JvExControls, JvXPCore, JvXPButtons, JvComponentBase, JvBalloonHint,
  JvXPCheckCtrls, MyEdit;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    MyEdit1: TMyEdit;
    _MaxValue: TJvNavPanelDivider;
    MaxValue: TMyEdit;
    _MinValue: TJvNavPanelDivider;
    MinValue: TMyEdit;
    _DecimalPlaces: TJvNavPanelDivider;
    DecimalPlaces: TMyEdit;
    CheckChars: TMyEdit;
    _CheckChars: TJvNavPanelDivider;
    DisplayFormat: TRadioGroup;
    JvNavPanelDivider1: TJvNavPanelDivider;
    JvXPButton2: TJvXPButton;
    JvXPButton3: TJvXPButton;
    _DisplayPrefix: TJvNavPanelDivider;
    DisplayPrefix: TMyEdit;
    _DisplaySuffix: TJvNavPanelDivider;
    DisplaySuffix: TMyEdit;
    JvXPCheckbox1: TJvXPCheckbox;
    JvXPCheckbox2: TJvXPCheckbox;
    procedure RadioButton1Click(Sender: TObject);
    procedure JvXPButton1Click(Sender: TObject);
    procedure JvXPButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvXPButton3Click(Sender: TObject);
    procedure SetDisplayPrefix(Sender: TObject);
    procedure SetDisplaySuffix(Sender: TObject);
    procedure SetCheckChars(Sender: TObject);
    procedure SetMinValue(Sender: TObject);
    procedure SetMaxValue(Sender: TObject);
    procedure SetDecimalPlaces(Sender: TObject);
    procedure SetZeroEmpty(Sender: TObject);
    procedure SetIsCurrency(Sender: TObject);
  private
    procedure AtualizaCampos;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  BalloonHint: TJvBalloonHint;  

implementation

{$R *.dfm}

procedure TForm1.SetDisplayPrefix(Sender: TObject);
begin
  if MyEdit1.DisplayPrefix <> DisplayPrefix.Text then
    MyEdit1.DisplayPrefix := DisplayPrefix.Text;
end;

procedure TForm1.SetDisplaySuffix(Sender: TObject);
begin
  if MyEdit1.DisplaySuffix <> DisplaySuffix.Text then
  MyEdit1.DisplaySuffix := DisplaySuffix.Text;
end;

procedure TForm1.SetCheckChars(Sender: TObject);
begin
  if MyEdit1.CheckChars <> CheckChars.Text then
    MyEdit1.CheckChars := CheckChars.Text;
end;

procedure TForm1.SetDecimalPlaces(Sender: TObject);
begin
  if MyEdit1.DecimalPlaces <> DecimalPlaces.AsInteger then
    MyEdit1.DecimalPlaces := DecimalPlaces.AsInteger;
end;

procedure TForm1.AtualizaCampos;
begin
  MinValue.AsNumeric := MyEdit1.MinValue;
  MaxValue.AsNumeric := MyEdit1.MaxValue;
  DecimalPlaces.AsInteger := MyEdit1.DecimalPlaces;
  DisplayPrefix.Text := MyEdit1.DisplayPrefix;
  DisplaySuffix.Text := MyEdit1.DisplaySuffix;
  CheckChars.Text := MyEdit1.CheckChars;  
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Left := (Screen.Width div 2) - (Width div 2);
  Top := (Screen.Height div 2) - (Height div 2) - 15;
  BalloonHint := TJvBalloonHint.Create(Application);
  BalloonHint.Options := [boPlaySound];
  AtualizaCampos;
end;

procedure TForm1.JvXPButton1Click(Sender: TObject);
begin
  case  MyEdit1.DisplayFormat of
    dfCheckChars: MyEdit1.CheckChars := CheckChars.Text;
    dfInteger, dfNumeric:
      begin
        MyEdit1.DecimalPlaces := DecimalPlaces.AsInteger;
        MyEdit1.MinValue := MinValue.AsNumeric;
        MyEdit1.MaxValue := MaxValue.AsNumeric;
      end;
  end;
  DecimalPlaces.AsInteger := MyEdit1.DecimalPlaces;
  MinValue.AsNumeric := MyEdit1.MinValue;
  MaxValue.AsNumeric := MyEdit1.MaxValue;
  MyEdit1.SetFocus;
end;

procedure TForm1.JvXPButton2Click(Sender: TObject);
begin
  BalloonHint.ActivateHint(TWinControl(Sender), Format('Get as Integer = %s',[IntToStr(MyEdit1.AsInteger)]), EmptyStr,1000);
end;

procedure TForm1.JvXPButton3Click(Sender: TObject);
begin
  BalloonHint.ActivateHint(TWinControl(Sender), Format('Get as Numeric = %s',[FloatToStr(MyEdit1.AsNumeric)]), EmptyStr,1000);
end;

procedure TForm1.SetIsCurrency(Sender: TObject);
begin
    MyEdit1.IsCurrency := JvXPCheckbox2.Checked;
    JvXPCheckbox2.Checked := MyEdit1.IsCurrency;
end;

procedure TForm1.SetZeroEmpty(Sender: TObject);
begin
  MyEdit1.ZeroEmpty := JvXPCheckbox1.Checked;
end;

procedure TForm1.SetMaxValue(Sender: TObject);
begin
  if   MyEdit1.MaxValue <> MaxValue.AsNumeric then
    MyEdit1.MaxValue := MaxValue.AsNumeric;
end;

procedure TForm1.SetMinValue(Sender: TObject);
begin
  if MyEdit1.MinValue <> MinValue.AsNumeric then
    MyEdit1.MinValue := MinValue.AsNumeric;
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin

  case  DisplayFormat.ItemIndex of
    0: MyEdit1.DisplayFormat := dfCheckChars;
    1: MyEdit1.DisplayFormat := dfInteger;
    2: MyEdit1.DisplayFormat := dfNumeric
  end;

  CheckChars.Enabled := False;
  DecimalPlaces.Enabled := False;
  MinValue.Enabled := False;
  MaxValue.Enabled := False;


  case MyEdit1.DisplayFormat of
    dfCheckChars:
      begin
        CheckChars.Enabled := True;
      end;
    dfInteger, dfNumeric:
      begin
        DecimalPlaces.Enabled := True;
        MinValue.Enabled := True;
        MaxValue.Enabled := True;
      end;
  end;

  AtualizaCampos;

  MyEdit1.SetFocus;

end;

end.
