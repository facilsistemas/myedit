unit MyEdit;

interface

uses
  System.SysUtils, Classes, Controls, StdCtrls, Graphics, Windows, Forms, Messages,
  Dialogs, Math, StrUtils, Mask, ExtCtrls;

const
  MaxInteger       = 2147483647;
  MinInteger       = -2147483647;
  MaxDecimalPlaces = 9;
  NegativeSignal   = '-';
  Null             = #0;
type
  TDisplayFormat = (dfCheckChars, dfInteger, dfNumeric);
  TMyEdit = class(TMaskEdit)
  private
    FAlignment: TAlignment;
    FAutoAlignment: Boolean;
    FCheckChars: string;
    FDecimalPlaces: Byte;
    Places : Integer;
    FDisplayFormat: TDisplayFormat;
    FDisplayPrefix: string;
    FDisplaySuffix: string;
    FEnterText: string;
    FInteger: Int64;
    FIsCurrency: Boolean;
    FMaxValue: Double;
    FMinValue: Double;
    FNumeric: Double;
    FOldText: string;
    FText: string;
    FZeroEmpty: Boolean;
    FFieldMapping: string;
    FOnSetText: TNotifyEvent;
    procedure SetAlignment(const Value: TAlignment);
    procedure SetCheckChars(const Value: string);
    procedure SetDecimalPlaces(const Value: Byte);
    procedure SetDisplayFormat(const Value: TDisplayFormat);
    procedure SetDisplayPrefix(const Value: string);
    procedure SetDisplaySuffix(const Value: string);
    procedure SetMaxValue(const Value: Double);
    procedure SetMinValue(const Value: Double);
    function  GetText: TCaption;
    procedure SetText(const Value: TCaption);
    procedure SetZeroEmpty(const Value: Boolean);    
    function  GetAsInteger: Int64;
    procedure SetAsInteger(const Value: Int64);
    function  GetAsNumeric: Double;
    procedure SetAsNumeric(const Value: Double);
    function  CountSubstring(const TheText: string; const FromText: string): Integer;
    procedure DisplayText(const Value: TCaption) overload;
    procedure DisplayText(const Value: Double) overload;
    procedure DisplayText(const Value: Int64) overload;
    function  MakeValid(const Value: string): string;
    procedure ChangeCursor(var Key: Char);
    function  TrimPrefixSuffix(const Value: string): string;
    procedure SetIsCurrency(const Value: Boolean);
    procedure SetFieldMapping(const Value: string);
    function  GetNumber(AText: string; AFromText: string = ','; AToText: string = '.'): string;    
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure SetName(const Value: TComponentName); override;
    procedure Change; override;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure KeyDown(var Key: Word;Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
  public
    constructor Create(AOwner: TComponent); override;
    property  AsInteger: Int64 read GetAsInteger write SetAsInteger;
    property  AsNumeric: Double read GetAsNumeric write SetAsNumeric;
    function  AsSql: string;
    function IsEmpty: Boolean;
    function GetOldText: string;    
    procedure WMPaste(var Msg: TMessage); message WM_PASTE;
    procedure WMClear(var Msg: TMessage); message WM_CLEAR;
    procedure WMUndo(var Msg: TMessage);
  published
    property  AutoAlignment: Boolean read FAutoAlignment write FAutoAlignment default True;
    property  Alignment: TAlignment read FAlignment write SetAlignment default taLeftJustify;
    property  CheckChars: string read FCheckChars write SetCheckChars;
    property  DecimalPlaces: Byte read FDecimalPlaces write SetDecimalPlaces default ZeroValue;
    property  DisplayFormat: TDisplayFormat read FDisplayFormat write SetDisplayFormat default dfCheckChars;
    property  DisplayPrefix: string read FDisplayPrefix write SetDisplayPrefix;
    property  DisplaySuffix: string read FDisplaySuffix write SetDisplaySuffix;
    property  IsCurrency: Boolean read FIsCurrency write SetIsCurrency default False;
    property  MaxValue: Double read FMaxValue write SetMaxValue;
    property  MinValue: Double read FMinValue write SetMinValue;
    property  FieldMapping: string read FFieldMapping write SetFieldMapping;
    property  Text: TCaption read GetText write SetText;
    property  ZeroEmpty: Boolean read FZeroEmpty write SetZeroEmpty default False;
    property OnSetText: TNotifyEvent read FOnSetText write FOnSetText;
  end;

procedure Register;

implementation

constructor TMyEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FAutoAlignment := True;
  FAlignment := taLeftJustify;
  FCheckChars := EmptyStr;
  FDecimalPlaces := ZeroValue;
  FDisplayFormat := dfCheckChars;
//  FDisplayFormat := dfNumeric;
  FDisplayPrefix := EmptyStr;
  FDisplaySuffix := EmptyStr;
  FInteger := ZeroValue;
  FIsCurrency := False;
  FNumeric := ZeroValue;
  FMaxValue := ZeroValue ;
  FMinValue := ZeroValue;
  FZeroEmpty := False;
end;

procedure TMyEdit.WMPaste(var Msg: TMessage);
begin
  if not ReadOnly then
  begin
    inherited;
    SetText(inherited Text);
  end;
end;

procedure TMyEdit.WMClear(var Msg: TMessage);
begin
  if not ReadOnly then
  begin
    inherited;
    SetText(inherited Text);
  end;
end;

procedure TMyEdit.WMUndo(var Msg: TMessage);
begin
  if not ReadOnly then
  begin
    inherited;
    SetText(inherited Text);
  end;
end;

procedure TMyEdit.SetAlignment(const Value: TAlignment);
begin
  if (FAlignment <> Value) then
  begin
    FAlignment := Value;
    RecreateWnd;
  end;
end;

procedure TMyEdit.SetCheckChars(const Value: string);
begin
  if (FDisplayFormat in [dfCheckChars]) then
  begin
    FCheckChars := Value;
    SetText(FText);
  end;
end;

procedure TMyEdit.SetDecimalPlaces(const Value: Byte);
begin
  Places := Value;
  if (FDisplayFormat in [dfNumeric]) then
  begin
    FDecimalPlaces := IfThen((Value=ZeroValue), FormatSettings.CurrencyDecimals, EnsureRange(Value, ZeroValue, MaxDecimalPlaces));
    SetText(FText);
  end;
end;

procedure TMyEdit.SetDisplayFormat(const Value: TDisplayFormat);
const
    Numbers = '-0123456789';
begin
  FDisplayFormat := Value;
  case FDisplayFormat of
    dfCheckChars:
      begin
        if FAutoAlignment then
          SetAlignment(taLeftJustify);
      end;
    dfInteger:
      begin
        if FAutoAlignment then
          SetAlignment(taCenter);
        FCheckChars := Numbers;
        MaxLength := ZeroValue;
      end;
    dfNumeric:
      begin
        if FAutoAlignment then
          SetAlignment(taCenter);
        FCheckChars := Numbers + FormatSettings.DecimalSeparator + FormatSettings.ThousandSeparator;

        SetDecimalPlaces(Places);

        if FDecimalPlaces = ZeroValue then
          FDecimalPlaces := FormatSettings.CurrencyDecimals;
        MaxLength := ZeroValue;          
      end;
  end;
  SetText(FText);
end;

procedure TMyEdit.SetDisplayPrefix(const Value: string);
begin
  if FDisplayPrefix <> Trim(Value)  then
  begin
    FDisplayPrefix := Value;
    SetText(FText);
  end;
end;

procedure TMyEdit.SetDisplaySuffix(const Value: string);
begin
  if FDisplaySuffix <> Trim(Value) then
  begin
    FDisplaySuffix := Trim(Value);
    SetText(FText);
  end;
end;

procedure TMyEdit.SetFieldMapping(const Value: string);
begin
  if (FFieldMapping <> Value) then
  begin
     FFieldMapping := Value;
  end;
end;

procedure TMyEdit.SetIsCurrency(const Value: Boolean);
begin
  if (FDisplayFormat = dfNumeric) and (FIsCurrency <> Value) then
  begin
    FIsCurrency := Value;
    SetText(FText);
  end;
end;

procedure TMyEdit.SetMaxValue(const Value: Double);
begin
  if FMaxValue <> Value then
  begin
    FMaxValue := EnsureRange(Value, MinInteger, MaxInteger);
    if FMinValue > FMaxValue then
      FMinValue := FMaxValue;
    SetText(FText);
  end;
end;

procedure TMyEdit.SetMinValue(const Value: Double);
begin
  if FMinValue <> Value then
  begin
    FMinValue := EnsureRange(Value, MinInteger, MaxInteger);
    if FMaxValue < FMinValue then
      FMaxValue := FMinValue;
    SetText(FText);
  end;
end;

procedure TMyEdit.SetName(const Value: TComponentName);
var
  ChangeText: Boolean;
begin
  ChangeText := (csSetCaption in ControlStyle) and
    not (csLoading in ComponentState) and (Name = Text) and
    ((Owner = nil) or not (Owner is TControl) or
    not (csLoading in TControl(Owner).ComponentState));
  inherited SetName(Value);
  if ChangeText then Text := Value;
  SetFieldMapping(Name);
end;

function TMyEdit.AsSql: string;
begin
  Result := EmptyStr;
  case FDisplayFormat of
    dfCheckChars: Result := QuotedStr(Trim(GetText));
    dfInteger: Result := FText ;
    dfNumeric: Result := ReplaceStr(FText, FormatSettings.DecimalSeparator, FormatSettings.ThousandSeparator);
  end;
end;

procedure TMyEdit.Change;
  var
    OldText: string;
    OldSelStart: Byte;
begin
  if (FDisplayFormat in [dfInteger, dfNumeric]) then
  begin
    //Pegando a posi��o atual do cursor
    OldSelStart := GetSelStart;
    //Pegando o texto neste instante
    OldText := inherited Text;
    //N�mero negativo
    if (OldText = NegativeSignal) then
     Exit;
    //Setando o novo valor
    SetText(OldText);
    //Setando a antiga posi��o do cursor
    SetSelStart(OldSelStart);
    if (GetSelStart = OldSelStart) then
      SetSelStart(OldSelStart +
                  CountSubstring(FormatSettings.ThousandSeparator,
                  Copy(Text, 1, SelStart)) -
                  CountSubstring(FormatSettings.ThousandSeparator,
                  Copy(OldText, 1, SelStart)));
  end;
  inherited Change;
end;

procedure TMyEdit.ChangeCursor(var Key: Char);
begin
  //S� pode ter uma ocorr�ncia
  if (Pos(key, inherited Text) > 0) then
  begin
    //Posiciona o cursor ap�s o caracter
    SetSelStart(Pos(key, inherited Text));
    //Anula o caracter
    Key := Null;
  end else
    //Posiciona o cursor na primeira casa
    SetSelStart(0);
end;

function TMyEdit.GetText: TCaption;
begin
  Result := inherited Text;
end;

function TMyEdit.IsEmpty: Boolean;
  var
  tmp: AnsiString;
begin

  if IsMasked then
  begin
    tmp := GetText;
    SetText(EmptyStr);
    Result := (tmp = GetText);
    SetText(tmp);
  end

  else

  Result := not(Trim(GetText)<>EmptyStr);

end;

procedure TMyEdit.SetText(const Value: TCaption);
  var
  EnterText: string;
begin
  EnterText := MakeValid(TrimPrefixSuffix(Value));
  case DisplayFormat of
    dfCheckChars: DisplayText(EnterText);
    dfInteger: DisplayText(StrToInt64Def(EnterText, ZeroValue));
    dfNumeric: DisplayText(StrToFloatDef(EnterText, ZeroValue));
  end;
  if (Assigned(FOnSetText) and Modified) then
    FOnSetText(Self);
end;

procedure TMyEdit.SetZeroEmpty(const Value: Boolean);
begin
  if FZeroEmpty <> Value then
  begin
    FZeroEmpty := Value;                  
    SetText(FText);
  end;
end;

{*------------------------------------------------------------------------------
  @Description Func�o para remover do conte�do do MyEdit uma string entre o Prefixo e / ou
    Sufixo setados no MyEdit
  @param  Value   conte�do do MyEdit
  @return string   Retorna string sem o Prefixo e o Sufixo setados para o MyEdit
  @Example
    .
    .
    .
      Prefix = '('
      Suffix = ')'
      ToText := TrimPrefixSuffix('(FromText)');
      ToText = 'FromText'
    .
    .
    .
------------------------------------------------------------------------------*}
function TMyEdit.TrimPrefixSuffix(const Value: string): string;
var
  A,B: Integer;
begin
  A := IfThen(StartsText(FDisplayPrefix, Value),(Length(FDisplayPrefix) + 1), 1);
  if FDisplaySuffix = EmptyStr then
    B := Length(Value)
  else
    B := IfThen(EndsText(FDisplaySuffix, Value),(Length(Value) - (Length(FDisplayPrefix) + Length(FDisplaySuffix))),Length(Value));

  Result := Copy(Value, A, B);
end;

function TMyEdit.GetAsInteger: Int64;
begin
  Result := FInteger;
end;

procedure TMyEdit.SetAsInteger(const Value: Int64);
begin
  DisplayText(Value);
end;

function TMyEdit.GetAsNumeric: Double;
begin
  Result := FNumeric;
end;

function TMyEdit.GetNumber(AText, AFromText, AToText: string): string;
  var
  I: Byte;
begin
  Result := EmptyStr;
  for I := 1 to Length(AText) do
  begin
    ///Pega se for n�mero
    if AText[I] in ['0'..'9', '-'] then
      Result := Result + AText[I]
    else
    ///Substitui AText por AFromText
    if AText[I] = AFromText then
      Result := Result + AToText
    else
    if AText[I] = AToText then  ///Mant�m o AToText
      Result := Result + AToText;
  end;
end;

function TMyEdit.GetOldText: string;
begin
  Result := FOldText;
end;

procedure TMyEdit.SetAsNumeric(const Value: Double);
begin
  DisplayText(Value);
end;

function TMyEdit.MakeValid(const Value: string): string;
  var
  I: Integer;
begin
  Result := EmptyStr;
  for I := 1 to Length(Value) do
    if (Pos(Value[I], FCheckChars) > 0) or (FCheckChars=EmptyStr) or (Value[I] in [',', '.'])then
      Result := Result + Value[I];
  Result := IfThen((FDisplayFormat in [dfInteger, dfNumeric]),
                    ReplaceStr(Result, FormatSettings.ThousandSeparator, EmptyStr),
                    Result);
end;

{*------------------------------------------------------------------------------
  @description    Fun��o para contar quantas ocorr�ncias de substrings existem
    em uma string
  @param TheText  Substring a ser comparada dentro da string
  @param FromText String a ser analisada
  @return Integer N�mero de ocorr�ncias
  @example
    .
    .
    .
      FromText := 'dividir para conquistar';
      TheText := 'd'
      Count := CountSubstring(TheText, FromText);
      Count = 2
    .
    .
    .          
------------------------------------------------------------------------------*}
function TMyEdit.CountSubstring(const TheText: string; const FromText: string): Integer;
  var
  Count: Integer;
begin
  Result := ZeroValue;
  Count := PosEx(TheText, FromText, 1);
  while Count > 0 do
  begin
    Inc(Result);
    Count := PosEx(TheText, FromText, Count + Length(TheText));
  end;
end;

procedure TMyEdit.DisplayText(const Value: TCaption);
begin

  FNumeric := RoundTo(EnsureRange(StrToFloatDef(GetNumber(Value, '.', ','), 0),
                      IfThen((FMinValue = FMaxValue), MinInteger, FMinValue),
                      IfThen((FMaxValue = FMinValue), MaxInteger, FMaxValue)),
                      NegativeValue * FDecimalPlaces);

  FInteger := Round(FNumeric);

  FText := Value;

  inherited

  Text := Format('%s%s%s', [IfThen(Focused, EmptyStr, FDisplayPrefix),
          FText, IfThen(Focused, EmptyStr, FDisplaySuffix)]);

end;

procedure TMyEdit.DisplayText(const Value: Double);
  var
  StrFmt: string;
begin
  FNumeric := RoundTo(EnsureRange(Value,
                      IfThen((FMinValue = FMaxValue), MinInteger, FMinValue),
                      IfThen((FMaxValue = FMinValue), MaxInteger, FMaxValue)),
                      NegativeValue * FDecimalPlaces);

  FInteger := Round(FNumeric);

  StrFmt := Format('%s,0.%%.%dd;-%s,0.%%.%dd',
                   [IfThen(Focused, EmptyStr, IfThen(FIsCurrency,
                    FormatSettings.CurrencyString, EmptyStr)) ,
                    FDecimalPlaces, IfThen(Focused, EmptyStr, EmptyStr),
                    FDecimalPlaces]);

  StrFmt := Format(StrFmt, [ZeroValue, ZeroValue, ZeroValue]);

  if Value < 0 then
    StrFmt := FormatFloat(StrFmt, FNumeric) + IfThen(Focused, EmptyStr, FDisplaySuffix)
  else
    StrFmt := IfThen(Focused, EmptyStr, FDisplayPrefix) + FormatFloat(StrFmt, FNumeric) +
                     IfThen(Focused, EmptyStr, FDisplaySuffix);

  FText := FloatToStr(FNumeric);

  inherited

  Text := IfThen(FZeroEmpty and (FNumeric = ZeroValue ),
                 IfThen(Focused, StrFmt, EmptyStr), StrFmt);

end;

procedure TMyEdit.DisplayText(const Value: Int64);
begin

  FNumeric := RoundTo(EnsureRange(Value,
                      IfThen((FMinValue = FMaxValue), MinInteger, FMinValue),
                      IfThen((FMaxValue = FMinValue), MaxInteger, FMaxValue)),
                      ZeroValue);

  FInteger := Round(FNumeric);

  FText := IntToStr(FInteger);

  inherited Text := IfThen(FZeroEmpty and (FInteger = ZeroValue ), EmptyStr,
                           Format('%s%.*n%s', [IfThen(Focused, EmptyStr, FDisplayPrefix),
                           ZeroValue, FNumeric, IfThen(Focused, EmptyStr, FDisplaySuffix)]));

end;

procedure TMyEdit.CreateParams(var Params: TCreateParams);
const
  Alinhamento: array[TAlignment] of DWORD = (ES_LEFT, ES_RIGHT, ES_CENTER);
begin
  inherited CreateParams(Params);
  Params.Style := Params.Style or Alinhamento[FAlignment];
end;

procedure TMyEdit.DoEnter;
begin
  SetText(inherited Text);
  FEnterText := GetText;
  SelectAll;
  inherited DoEnter;
end;

procedure TMyEdit.DoExit;
  var
  Result: Boolean;
begin
  FOldText := FEnterText;
  Result := (FEnterText <> GetText);
  SetText(inherited Text);
  Modified := Result;
  inherited DoExit
end;

procedure TMyEdit.KeyDown(var Key: Word; Shift: TShiftState);

begin

  inherited KeyDown(Key, Shift);

  if ((FDisplayFormat = dfInteger) or (FDisplayFormat =  dfNumeric)) and (GetText <> EmptyStr) then

  begin

    case Key of

      VK_BACK:

      begin
      
        if (AsNumeric = ZeroValue) then

          SetSelStart(ZeroValue)

        else

        if (GetText[GetSelStart] = FormatSettings.DecimalSeparator) then

          Key := VK_LEFT;

      end;

      VK_DELETE:

      begin

        if (GetText[GetSelStart + 1] = FormatSettings.DecimalSeparator) or
           (GetText[GetSelStart + 1] = FormatSettings.ThousandSeparator)  then

          Key := VK_RIGHT;

      end;

      VK_RETURN:
      begin
        SelectAll;
      end;

    end;
      
  end;

end;

procedure TMyEdit.KeyPress(var Key: Char);

begin

  if ((Pos(Key, FCheckChars) > 0) or (FCheckChars=EmptyStr)) or (Key <= #32) then

  begin

    if (FDisplayFormat = dfInteger) or (FDisplayFormat = dfNumeric) then

    begin

      if (Key = FormatSettings.DecimalSeparator) or (Key = FormatSettings.ThousandSeparator) then

      begin

        Key := FormatSettings.DecimalSeparator;

        ChangeCursor(Key);

      end

      else

      if (Key = NegativeSignal) and (SelLength = ZeroValue) then

      begin

        if (FMinValue < ZeroValue) or (FMinValue = FMaxValue) then

        begin

          ChangeCursor(Key);

          if (GetAsNumeric = ZeroValue)  then
            SetSelLength(MaxInt);

        end

        else
          Key := Null;
      end
      else
      if (GetAsNumeric = ZeroValue)  then
        SetSelLength(MaxInt);

    end;

  end else Key := Null;

  inherited KeyPress(Key);

end;

procedure Register;
begin
  RegisterComponents('MyEdit', [TMyEdit]);
end;

end.
