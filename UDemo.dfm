object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'My Edit'
  ClientHeight = 536
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MaxHeight = 570
  Constraints.MaxWidth = 800
  Constraints.MinHeight = 570
  Constraints.MinWidth = 800
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Verdana'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 16
  object JvNavPanelDivider1: TJvNavPanelDivider
    Left = 8
    Top = 8
    Width = 776
    Height = 520
    Caption = 'pedrourbano.mg@gmail.com'
  end
  object GroupBox1: TGroupBox
    Left = 188
    Top = 87
    Width = 417
    Height = 162
    Caption = 'Configurations'
    TabOrder = 1
    TabStop = True
    object _MaxValue: TJvNavPanelDivider
      Left = 147
      Top = 24
      Caption = 'Max Value'
      Enabled = False
    end
    object _MinValue: TJvNavPanelDivider
      Left = 16
      Top = 24
      Caption = 'Min Value'
      Enabled = False
    end
    object _DecimalPlaces: TJvNavPanelDivider
      Left = 278
      Top = 24
      Caption = 'Decimal Places'
      Enabled = False
    end
    object _CheckChars: TJvNavPanelDivider
      Left = 278
      Top = 79
      Caption = 'CheckChars'
    end
    object _DisplayPrefix: TJvNavPanelDivider
      Left = 16
      Top = 79
      Caption = 'Display Prefix'
    end
    object _DisplaySuffix: TJvNavPanelDivider
      Left = 147
      Top = 79
      Caption = 'Display Suffix'
    end
    object MaxValue: TMyEdit
      Left = 147
      Top = 49
      Width = 125
      Height = 24
      Enabled = False
      TabOrder = 1
      Text = '0,00'
      OnExit = SetMaxValue
      Alignment = taCenter
      CheckChars = '-0123456789,.'
      DecimalPlaces = 2
      DisplayFormat = dfNumeric
      MaxValue = 2147483647.000000000000000000
      MinValue = -2147483647.000000000000000000
    end
    object MinValue: TMyEdit
      Left = 16
      Top = 49
      Width = 125
      Height = 24
      Enabled = False
      TabOrder = 0
      Text = '0,00'
      OnExit = SetMinValue
      Alignment = taCenter
      CheckChars = '-0123456789,.'
      DecimalPlaces = 2
      DisplayFormat = dfNumeric
      MaxValue = 2147483647.000000000000000000
      MinValue = -2147483647.000000000000000000
    end
    object DecimalPlaces: TMyEdit
      Left = 278
      Top = 49
      Width = 125
      Height = 24
      Enabled = False
      TabOrder = 2
      Text = '0'
      OnExit = SetDecimalPlaces
      Alignment = taCenter
      CheckChars = '-0123456789'
      DisplayFormat = dfInteger
      MaxValue = 1000000000.000000000000000000
      MinValue = -2147483647.000000000000000000
    end
    object CheckChars: TMyEdit
      Left = 278
      Top = 104
      Width = 125
      Height = 24
      TabOrder = 3
      OnExit = SetCheckChars
      MaxValue = 2147483647.000000000000000000
      MinValue = -2147483647.000000000000000000
    end
    object DisplayPrefix: TMyEdit
      Left = 16
      Top = 104
      Width = 125
      Height = 24
      TabOrder = 4
      OnExit = SetDisplayPrefix
    end
    object DisplaySuffix: TMyEdit
      Left = 147
      Top = 104
      Width = 125
      Height = 24
      TabOrder = 5
      OnExit = SetDisplaySuffix
    end
    object JvXPCheckbox1: TJvXPCheckbox
      Left = 16
      Top = 134
      Width = 125
      Height = 17
      Caption = 'ZeroEmpty'
      TabOrder = 6
      OnClick = SetZeroEmpty
    end
    object JvXPCheckbox2: TJvXPCheckbox
      Left = 147
      Top = 134
      Width = 125
      Height = 17
      Caption = 'Is Currency?'
      TabOrder = 7
      OnClick = SetIsCurrency
    end
  end
  object GroupBox3: TGroupBox
    Left = 16
    Top = 16
    Width = 753
    Height = 65
    Caption = 'Hello My Edit!'
    TabOrder = 0
    object MyEdit1: TMyEdit
      Left = 16
      Top = 25
      Width = 465
      Height = 24
      TabOrder = 0
      AutoAlignment = False
      Alignment = taCenter
    end
    object JvXPButton2: TJvXPButton
      Left = 487
      Top = 25
      Width = 122
      Height = 24
      Caption = 'Get as integer'
      TabOrder = 1
      AutoGray = False
      OnClick = JvXPButton2Click
    end
    object JvXPButton3: TJvXPButton
      Left = 615
      Top = 25
      Width = 122
      Height = 24
      Caption = 'Get as numeric'
      TabOrder = 2
      AutoGray = False
      OnClick = JvXPButton3Click
    end
  end
  object DisplayFormat: TRadioGroup
    Left = 188
    Top = 287
    Width = 417
    Height = 138
    Caption = 'DisplayFormat'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'CheckChars'
      'Integer'
      'Numeric')
    TabOrder = 2
    TabStop = True
    OnClick = RadioButton1Click
  end
end
